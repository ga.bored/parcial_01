// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
      apiKey: "AIzaSyChTAg3_79fiODOqA4wLd5DG4DuP5e40H4",
      authDomain: "data-201b9.firebaseapp.com",
      databaseURL: "https://data-201b9.firebaseio.com",
      projectId: "data-201b9",
      storageBucket: "data-201b9.appspot.com",
      messagingSenderId: "779889896850",
      appId: "1:779889896850:web:71ff0c9b8457bb4c36254b"
    }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
