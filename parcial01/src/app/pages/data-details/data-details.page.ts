import { Component, OnInit } from '@angular/core';
import { Data } from '../../models/data';
import { DataService } from '../../services/data.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-data-details',
  templateUrl: './data-details.page.html',
  styleUrls: ['./data-details.page.scss'],
})
export class DataDetailsPage implements OnInit {

  data : Data = {

    name: '',
    country: '',
    diet: '',
    habitat: '',
    pic: ''
  };
  dataId = null;

  constructor(private route: ActivatedRoute, private nav: NavController,
    private dataService: DataService, private loadingController: LoadingController) {

  }
  ngOnInit() {
    this.dataId = this.route.snapshot.params['id'];
    if (this.dataId) {
      this.loadData();
    }
  }
  async loadData() {
    const loading = await this.loadingController.create({
      message: "Cargando..."
    });
    await loading.present();
    this.dataService.getData(this.dataId).subscribe(res => {
      loading.dismiss();
      this.data = res;
      console.log(this.data);
    })
  }

  

}
