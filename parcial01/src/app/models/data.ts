export interface Data {
    id?:string;
    name:string;
    pic:string;
    country:string;
    habitat:string;
    diet: string;

}
